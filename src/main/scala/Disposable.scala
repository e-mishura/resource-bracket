trait Disposable {
  def dispose: Unit
}
