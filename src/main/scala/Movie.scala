import java.time.Year

case class Movie(id: Int, name: String, yearReleased: Year)

/** API to fetch Movies */
trait Api {

  /** Given a page number, fetches movies on that page (empty if page is past last page) */
  def fetchBatch(page: Int): Iterator[Movie]
}




object Movie extends App {

  def namesByYear(api: Api): Map[Year, Set[String]] =
    movieSrc(api)
      .groupBy(_.yearReleased)
      .mapValues( _ map {_.name} toSet )

  import StreamUtil._
  /** aggregates in constant memory */
  def namesByYear2(api: Api): Map[Year, Set[String]] =
    movieSrc(api)
      .groupByWith(
        _.yearReleased,
        Set.empty[String],
        (acc: Set[String], movie: Movie) => acc + movie.name)


  private def movieSrc(api: Api, fromPage: Int = 0): Stream[Movie] =
    movieBatchSrc(api, fromPage)
      .flatMap(_.toStream)

  private def movieBatchSrc(api: Api, page: Int): Stream[Iterator[Movie]] =
    api.fetchBatch(page) match {
      case Iterator.empty => Stream.empty
      case batch => batch #:: movieBatchSrc(api, page+1)
    }


//  val db = new MovieDb(3)
//  val movies = movieSrc(db)
//  movies.foreach(println)


  val db = new MovieDb(3000000)
  val byYear = namesByYear2(db)
  println(byYear)
}


class MovieDb(pages: Int) extends Api {

  /** Given a page number, fetches movies on that page (empty if page is past last page) */
  override def fetchBatch(page: Int): Iterator[Movie] =
    if(page >= pages)
      Iterator.empty
    else
      List(
        Movie(page*10 +1, "Movie 1", Year.of(2019)),
        Movie(page*10 +2, "Movie 2", Year.of(2019)),
        Movie(page*10 +3, "Movie 1", Year.of(2019)),
        Movie(page*10 +4, "Movie 3", Year.of(2018)),
        Movie(page*10 +5, "Movie 4", Year.of(2018)),
      ).toIterator
}

object StreamUtil {

  implicit class StreamOps[A](stream: Stream[A]) {

    def groupByWith[K, V](key: A => K, z: => V, aggregate: (V, A) => V): Map[K, V] =
      stream.foldLeft(Map.empty[K, V]) { (acc, a) =>
        val k = key(a)
        val prevV = acc.getOrElse(k, z)
        val newV = aggregate(prevV, a)
        acc.updated(k, newV)
      }
  }

}

