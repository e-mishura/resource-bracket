case class MyRes[T](private val value: T, name: String) extends Disposable {
  private var disposed: Boolean = false

  def get: T = if(!disposed) value else throw new Exception(s"$name is disposed")

  override def dispose: Unit = {
    println(s"Disposing $name")
    disposed = true
  }
}
