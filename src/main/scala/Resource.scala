

case class Resource[A](value: A, resources: List[Disposable]) {
  def map[B](f: A => B): Resource[B] = Resource(f(value), resources)
  def flatMap[B](f: A => Resource[B]): Resource[B] = {
    val rb = f(value)
    Resource(rb.value, rb.resources ++ resources)
  }

 def foreach[U](f: A => U): Unit = {
    f(value)
    resources.foreach(_.dispose)
  }
}

object Resource {
  def apply[A <: Disposable](a: A): Resource[A] = Resource(a, List(a))
}

object Resources extends App {

  val resource1 = MyRes(5, "fiver")
  val resource2 = MyRes(10, "tenner")

  val res = for {
    r1 <- Resource(resource1)
    rr = r1.get + 100
    r2 <- Resource(resource2)
  }  println(s" result: ${rr + r2.get}")
}


