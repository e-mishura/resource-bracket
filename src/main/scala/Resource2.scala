case class Resource2[A](allocate: () => (A, ()=>Unit)) {

  def map[B](f: A => B): Resource2[B] = {
    val allocateB = () => {
      val (a, release) = allocate()
      (f(a), release)
    }
    Resource2(allocateB)
  }
      def flatMap[B](f: A => Resource2[B]): Resource2[B] = {
        val allocateB = () => {
          val (a, releaseA) = allocate()
          val resB = f(a)
          val (b, releaseB) = resB.allocate()
          (b, () => {releaseB(); releaseA()})
        }
        Resource2(allocateB)
  }

  def use[B](f: A=>B): B = {
    val(a, release) = allocate()
    try {
      f(a)
    } finally {
      release()
    }
  }

  def foreach[U](f: A=> U): Unit = use(f)
}

object Resource2 extends App {

  def fromDisposable[A <: Disposable](a: A): Resource2[A] = Resource2(() => (a, () => a.dispose))

  val resource1 = MyRes(5, "fiver")
  val resource2 = MyRes(10, "tenner")

  val res = for {
    r1 <- Resource2.fromDisposable(resource1)
    rr = r1.get + 100
    r2 <- Resource2.fromDisposable(resource2)
  }  println(s" result: ${rr + r2.get}")
}